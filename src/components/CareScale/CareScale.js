function CareScale({scaleValue, careType}) {
  const range = [1, 2, 3]
  const scaleType = (careType === 'light' ? '☀️' : '💧')
  return (
    <div>
      {range.map((element)=>(
        scaleValue >= element && <span key={element}>{ scaleType }</span>
      ))}
    </div>
  )
}
  
export default CareScale