import { plantList } from "../../data/plantList"
import PlantItem from "./PlantItem/PlantItem"
import "./ShoppingList.css"

function ShoppingList({cart, updateCart}) {
    let categoryList = []

    plantList.forEach((plant) => {
        if(!categoryList.includes(plant.category)) categoryList.push(plant.category)
    })

    return (
        <div className="shopping-list">
            <h3>Catégories</h3>
            <ul className="category-list">
                {categoryList.map((name) => (
                    <li className="categorie-item" key={name}>{name}</li>
                ))}
            </ul>

            <h3>Plantes</h3>
            <ul className="plant-list">
                {plantList.map((plant) => (
                    <PlantItem cart={cart} updateCart={updateCart} key={plant.id} name={plant.name} cover={plant.cover} id={plant.id} light={plant.light} water={plant.water} price={plant.price}>
                        { plant.isBestSale && <span>🔥</span>}
                        { plant.isSpecialOffer && <div className="soldes-text">SOLDES</div>}
                    </PlantItem>
                ))}
            </ul>
        </div>
    )
}

export default ShoppingList