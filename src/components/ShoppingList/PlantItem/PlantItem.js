import CareScale from "../../CareScale/CareScale"
import "./PlantItem.css"

function handleAddToCart(cart, updateCart, plantName, plantPrice, plantId) {
    console.log(`Vous avez acheté 1 ${plantName} ! Très bon choix ! 🌱✨`)
    updateCart(cart.concat({name: plantName, price: plantPrice, id: plantId}))
}

function PlantItem({cart, updateCart, name, cover, id, light, water, children, price}) {
    return (
        <li className="plant-item">
            <img className='plant-item-cover' src={cover} alt={`${name} cover`} />
            { name }
            <div>
                <CareScale careType="light" scaleValue={light}/>
                <CareScale careType="water" scaleValue={water}/>
            </div>
            {children}
            <button className="add-button" onClick={() => handleAddToCart(cart, updateCart, name, price, id)}>Ajouter</button>
            <div className="price-text">{Number.parseFloat(price).toFixed(2)} €</div>
        </li>
    )
}

export default PlantItem