import Banner from "./Banner/Banner"
import Cart from "./Cart/Cart"
import QuestionForm from "./QuestionForm/QuestionForm"
import ShoppingList from "./ShoppingList/ShoppingList"
import "./App.css"
import { useState } from "react"

function App() {
  const [cart, updateCart] = useState([])
  return (
    <div className="app">
      <Banner/>
      <div className="content">
        <div className="left">
          <Cart cart={cart} updateCart={updateCart}/>
        </div>
        <div className="right">
          <ShoppingList cart={cart} updateCart={updateCart}/>
          <QuestionForm/>
        </div>
      </div>
    </div>
  )
}

export default App
