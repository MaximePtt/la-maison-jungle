import { useState, useEffect } from 'react'
import './Cart.css'

function Cart({cart, updateCart}) {
	const [isOpen, setIsOpen] = useState(true)
	let cartPrice = 0
	
	cart.forEach(plant=>cartPrice += plant.price)

	let total = Number.parseFloat(cartPrice).toFixed(2)
	// A chaque ajout ou suppression, stocker le panier dans le local storage lorsqu'il change 
	useEffect(() => {
		document.title = `LMJ: ${total}€ d'achats`
		if(cart.length > 0){ // 
			localStorage.setItem("cart", JSON.stringify(cart))
		}
	}, [cart])

	// Au chargement, récupérer le panier dans le local storage
	useEffect(() => {
		updateCart(JSON.parse(localStorage.getItem("cart")))
	}, [])

	// Comptage du nombre d'occurences de chacun
	let plantsWithCount = []
	cart.forEach(plant=>{
		if(!plantsWithCount.find(p => p.id === plant.id)){
			let nbOcc = 0
			cart.forEach(p=>{
				if(p.id === plant.id) nbOcc++
			})
			plantsWithCount.push({...plant, count: nbOcc})
		}
	})

	return isOpen ? (
		<div className='cart'>
			<button
				className='cart-button'
				onClick={() => setIsOpen(false)}
			>
				Fermer le panier
			</button>
			<h2>Panier</h2>
			<div className='plants-in-cart-container'>
			{
				plantsWithCount.length?
				plantsWithCount.map(plant=>{
					return (
						<div className='plant-in-cart' key={plant.id}>
							<div>{plant.name}</div>
							<div className='price-quantity'>
								<div>{"x" + plant.count}</div>
								<div>{ total } €</div>
							</div>
						</div>
					)
				}):
				<div>Votre panier est vide</div>
			}
			</div>
			<h3 className=''>Total : {Number.parseFloat(cartPrice).toFixed(2)}€</h3>
            <button className='cart-button' onClick={() => updateCart([])}>Vider le panier</button>
		</div>
	) : (
		<div className='cart-closed'>
			<button
				className='cart-open-button'
				onClick={() => setIsOpen(true)}
			>
				Ouvrir le Panier
			</button>
		</div>
	)
}

export default Cart