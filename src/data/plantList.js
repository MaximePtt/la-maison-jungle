export const plantList = [
	{
		name: 'monstera',
		category: 'classique',
		id: '1ed',
		isBestSale: true,
		isSpecialOffer: true,
		light: 3,
		water: 2,
		cover: "https://raw.githubusercontent.com/OpenClassrooms-Student-Center/7008001-Debutez-avec-React/P2C4-Solution/src/assets/monstera.jpg",
		price: 5.56,
	},
	{
		name: 'ficus lyrata',
		category: 'classique',
		id: '2ab',
		isBestSale: false,
		isSpecialOffer: false,
		light: 2,
		water: 1,
		cover: "https://raw.githubusercontent.com/OpenClassrooms-Student-Center/7008001-Debutez-avec-React/P2C4-Solution/src/assets/monstera.jpg",
		price: 9.99,
	},
	{
		name: 'pothos argenté',
		category: 'classique',
		id: '3sd',
		isBestSale: false,
		isSpecialOffer: true,
		light: 3,
		water: 3,
		cover: "https://raw.githubusercontent.com/OpenClassrooms-Student-Center/7008001-Debutez-avec-React/P2C4-Solution/src/assets/monstera.jpg",
		price: 20.00,
	},
	{
		name: 'yucca',
		category: 'classique',
		id: '4kk',
		isBestSale: false,
		isSpecialOffer: false,
		light: 3,
		water: 2,
		cover: "https://raw.githubusercontent.com/OpenClassrooms-Student-Center/7008001-Debutez-avec-React/P2C4-Solution/src/assets/monstera.jpg",
		price: 12.50,
	},
	{
		name: 'olivier',
		category: 'extérieur',
		id: '5pl',
		isBestSale: false,
		isSpecialOffer: true,
		light: 1,
		water: 2,
		cover: "https://raw.githubusercontent.com/OpenClassrooms-Student-Center/7008001-Debutez-avec-React/P2C4-Solution/src/assets/monstera.jpg",
		price: 8.20,
	},
	{
		name: 'géranium',
		category: 'extérieur',
		id: '6uo',
		isBestSale: false,
		isSpecialOffer: false,
		light: 2,
		water: 1,
		cover: "https://raw.githubusercontent.com/OpenClassrooms-Student-Center/7008001-Debutez-avec-React/P2C4-Solution/src/assets/monstera.jpg",
		price: 1.00,
	},
	{
		name: 'basilique',
		category: 'extérieur',
		id: '7ie',
		isBestSale: true,
		isSpecialOffer: false,
		light: 3,
		water: 1,
		cover: "https://raw.githubusercontent.com/OpenClassrooms-Student-Center/7008001-Debutez-avec-React/P2C4-Solution/src/assets/monstera.jpg",
		price: 5.00,
	},
	{
		name: 'aloe',
		category: 'plante grasse',
		id: '8fp',
		isBestSale: false,
		isSpecialOffer: false,
		light: 1,
		water: 1,
		cover: "https://raw.githubusercontent.com/OpenClassrooms-Student-Center/7008001-Debutez-avec-React/P2C4-Solution/src/assets/monstera.jpg",
		price: 4.80,
	},
	{
		name: 'succulente',
		category: 'plante grasse',
		id: '9vn',
		isBestSale: false,
		isSpecialOffer: false,
		light: 1,
		water: 3,
		cover: "https://raw.githubusercontent.com/OpenClassrooms-Student-Center/7008001-Debutez-avec-React/P2C4-Solution/src/assets/monstera.jpg",
		price: 9.67,
	}
]